# Hooktube Blacked +

## About
This theme applies to hooktube.com and is an expansion of the Hooktube Blacked theme. Some fonts and page elements have been compacted for convenience.

## Links
Userstyles.org page: https://userstyles.org/styles/149287/hooktube-blacked

## Screenshot
![theme screenshot](hooktube.png)

## Installation
### Step 1:
Install the Stylish or Stylus extension for Firefox or Chrome. Then, either:

### Step 2:
Install from userstyles (https://userstyles.org/styles/149287/hooktube-blacked)  
OR  
Import the *.css file into Stylish or Stylus manually
